DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 
DECLARE widget_ids STRING DEFAULT '(más_opciones_similares|varias_personas_después_miran|varias personas después miran|más opciones similares)';


-- insert into `core-search-dev.recommendation_dashboard.revenue_page`
create table if not exists `core-search-dev.recommendation_dashboard.revenue_page` PARTITION BY day as
with click_raw as(
    select distinct
    day,
    os_type,
    concat(visit_high, visit_low) as visitor_id,
    product_id
    FROM `falabella-search-prod.recommendation.facl_widget_click`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    -- AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

revenue_order as (
    SELECT  
    day,
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    purchase_id,
    product_id,
    sum(quantity) quantity,
    sum(safe_multiply(price, quantity)) as revenue
    FROM `falabella-search-prod.recommendation.facl_omniture_orders` 
    WHERE day BETWEEN DATE(start_date) and DATE(end_date)
    group by 1,2,3,4
),


widget_orders as(
    select distinct
    t1.day,
    t1.os_type,
    concat(visit_high, visit_low) as visitor_id,
    t1.product_id
    FROM `falabella-search-prod.recommendation.facl_widget_orders` t1
    inner join click_raw t2
    on (t1.day=t2.day and t1.os_type=t2.os_type and concat(t1.visit_high, t1.visit_low)=t2.visitor_id and t1.product_id=t2.product_id)
    WHERE source='App'
    AND ((t1.os_type = 'Android' and  app_version >= '2.3.0') or (t1.os_type = 'iOS' and  app_version >= '2.2.16'))
    -- AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND t1.day BETWEEN DATE(start_date) AND DATE(end_date)
),


widget_orders_purchase_id as(
  select t1.day, t1.os_type, t1.visitor_id, t1.product_id, t2.purchase_id
  FROM widget_orders t1
  inner join revenue_order t2
  on (t1.day=t2.day and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
),


total_orders_revenue as(
  select t1.day, t1.os_type, count(distinct t1.purchase_id) as total_orders, sum(revenue) as total_revenue, sum(quantity) total_quantity_bought
  FROM (
    select distinct 
    day, os_type, visitor_id, purchase_id 
    from widget_orders_purchase_id
  ) t1
  inner join revenue_order t2
  on (t1.day=t2.day and t1.visitor_id=t2.visitor_id and t1.purchase_id=t2.purchase_id)
  group by 1,2
),


widget_orders_revenue as(
  select t1.day, t1.os_type, count(t1.product_id) as total_reco_items, sum(revenue) as reco_revenue, sum(quantity) widget_quantity_bought
  FROM (
    select distinct 
    day, os_type, visitor_id, product_id
    from widget_orders_purchase_id
  )t1
  inner join revenue_order t2
  on (t1.day=t2.day and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  group by 1,2
)


select 
t1.day, 
t1.os_type, 
total_orders,
total_revenue,
total_quantity_bought,
total_reco_items,
reco_revenue,
widget_quantity_bought
from total_orders_revenue t1
left join widget_orders_revenue t2 on (t1.day=t2.day and t1.os_type=t2.os_type)
