DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 


-- insert into `core-search-dev.recommendation_dashboard.app_view`
create table if not exists `core-search-dev.recommendation_dashboard.app_view` PARTITION BY day as
with cte_appmetrics as ( 
    select distinct 
    concat(post_visid_high, post_visid_low) as visitor_id, 
    case when LOWER(user_agent) like '%iphone%' then 'iOS' else 'Android' end as os_type,
    day,
    app_page_type, 
    coalesce(purchaseid, post_purchaseid) as purchase_id,
    product_list 
    from `falabella-search-prod.relevancy_matrices.facl_raw_omniture` 
    where ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
    (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
    and day between DATE(start_date) AND DATE(end_date)
),


appvisit as ( 
    select day, os_type, count(*) as total_visits 
    from (
        select distinct day, os_type, visitor_id from cte_appmetrics
    ) 
    group by 1,2
), 


pdpvisits as ( 
    select day, os_type, count(*) as pdpvisit
    from ( 
    select distinct day, os_type, visitor_id from cte_appmetrics 
    where app_page_type = 'prodView' or app_page_type='pdp_visible_components'
    ) 
    group by 1,2
), 


appcartadd as ( 
    select day, os_type, count(*) as cartadd 
    from ( 
    select distinct day, os_type, visitor_id from cte_appmetrics
    where app_page_type = 'scAdd' 
    )
    group by 1,2
), 


apporder as (
    select day, os_type, count(*) as orders -- count of unique purchase ids
    from ( 
        select distinct day, os_type, purchase_id from cte_appmetrics
        where app_page_type = 'Purchase' and purchase_id is not null and product_list is not null 
    ) 
    group by 1, 2
), 


revenueorders as ( 
    select t1.day, os_type, t1.purchase_id, revenue, quantity
    from ( 
        select distinct day, os_type, purchase_id from cte_appmetrics
        where app_page_type = 'Purchase' and purchase_id is not null and product_list is not null 
    )t1
    inner join (
        select day, purchase_id, sum(safe_multiply(price, quantity)) as revenue, sum(quantity) as quantity
        from `falabella-search-prod.recommendation.facl_omniture_orders` 
        where day between DATE(start_date) AND DATE(end_date)
        group by 1, 2
    )t2
    on (t1.day=t2.day and t1.purchase_id=t2.purchase_id)
), 


apprevenue as ( 
    select day, os_type, sum(revenue) as Revenue, sum(quantity) as total_quantity_bought 
    from revenueorders
    group by 1, 2
)

select 
appvisit.day as day, 
appvisit.os_type as os_type,
total_visits,
pdpvisit,
cartadd,
orders as total_orders,
revenue,
total_quantity_bought
from appvisit
left join pdpvisits on (appvisit.day=pdpvisits.day and appvisit.os_type=pdpvisits.os_type)
left join appcartadd on (appvisit.day=appcartadd.day and appvisit.os_type=appcartadd.os_type)
left join apporder on (appvisit.day=apporder.day and appvisit.os_type=apporder.os_type)
left join apprevenue on (appvisit.day=apprevenue.day and appvisit.os_type=apprevenue.os_type)
