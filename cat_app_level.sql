DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 


-- insert into `core-search-dev.recommendation_dashboard.cat_app_level`
create table if not exists `core-search-dev.recommendation_dashboard.cat_app_level` PARTITION BY day as
with cte_appmetrics_raw as(
    select  
    day,
    case when LOWER(user_agent) like '%iphone%' then 'iOS' else 'Android' end as os_type,
    concat(post_visid_high, post_visid_low) as visitor_id, 
    app_page_type, 
    coalesce(purchaseid, post_purchaseid) as purchase_id,
    SPLIT(product_list, ',') product_list_split
    from `falabella-search-prod.relevancy_matrices.facl_raw_omniture` 
    where ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
    (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
    and day between DATE(start_date) AND DATE(end_date)
),


cte_appmetrics as(
    select distinct
    day,
    os_type,
    visitor_id, 
    app_page_type, 
    purchase_id,
    SPLIT(product_list_split, ';')[safe_offset(1)] as product_id
    from cte_appmetrics_raw, UNNEST(product_list_split) product_list_split
),


appvisit as ( 
    select day, os_type, product_id, count(*) as total_visits 
    from (
        select distinct day, os_type, visitor_id, product_id from cte_appmetrics
    ) 
    group by 1,2,3
), 


pdpvisits as ( 
    select day, os_type, product_id, count(*) as pdpvisit
    from ( 
    select distinct day, os_type, visitor_id, product_id from cte_appmetrics 
    where app_page_type = 'prodView' or app_page_type='pdp_visible_components'
    ) 
    group by 1,2,3
), 


appcartadd as ( 
    select day, os_type, product_id, count(*) as cartadd 
    from ( 
    select distinct day, os_type, visitor_id, product_id from cte_appmetrics
    where app_page_type = 'scAdd' 
    )
    group by 1,2,3
), 


apporders as ( 
    select t1.day, os_type, t1.product_id, orders, revenue, quantity
    from ( 
        select distinct day, os_type, product_id from cte_appmetrics
        where app_page_type = 'Purchase' and purchase_id is not null and product_id is not null 
    )t1
    inner join (
        select day, product_id, count(distinct purchase_id) as orders, sum(safe_multiply(price, quantity)) as revenue, sum(quantity) as quantity
        from `falabella-search-prod.recommendation.facl_omniture_orders` 
        where day between DATE(start_date) AND DATE(end_date)
        group by 1, 2
    )t2
    on (t1.day=t2.day and t1.product_id=t2.product_id)
), 


cat_mapping as (
    select t1.product_id, t2.category 
    from (
        select distinct product_id from appvisit
    )t1
    inner join (
        select distinct 
        product_id, 
        split(split(PRODUCT_L0_CATEGORY_PATHS, '||')[safe_offset(1)], ',')[safe_offset(0)] as category 
        from `tc-sc-bi-bigdata-dtl-fcom-prd.cl_entities.facl_prod_feed`
        WHERE DATE(_PARTITIONTIME) = DATE(end_date)
    ) t2
    on (t1.product_id=t2.product_id)
)


select
t1.day,
t1.os_type,
category,
t1.product_id,
total_visits,
pdpvisit,
cartadd,
orders,
revenue, 
quantity
from appvisit t1
left join pdpvisits t2 on (t1.day = t2.day and t1.os_type = t2.os_type and t1.product_id = t2.product_id)
left join appcartadd t3 on (t1.day = t3.day and t1.os_type = t3.os_type and t1.product_id = t3.product_id)
left join apporders t4 on (t1.day = t4.day and t1.os_type = t4.os_type and t1.product_id = t4.product_id)
left join cat_mapping t5 on (t1.product_id = t5.product_id)
where category is not null and total_visits is not null 
