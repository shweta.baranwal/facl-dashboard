DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 
DECLARE widget_ids STRING DEFAULT '(más_opciones_similares|varias_personas_después_miran|varias personas después miran|más opciones similares)';


-- insert into `core-search-dev.recommendation_dashboard.cat_reco_level`
create table if not exists `core-search-dev.recommendation_dashboard.cat_reco_level` PARTITION BY day as
with cte_appmetrics_raw as(
    select  
    day,
    case when LOWER(user_agent) like '%iphone%' then 'iOS' else 'Android' end as os_type,
    concat(post_visid_high, post_visid_low) as visitor_id, 
    app_page_type, 
    SPLIT(product_list, ',') product_list_split
    from `falabella-search-prod.relevancy_matrices.facl_raw_omniture` 
    where ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
    (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
    and day between DATE(start_date) AND DATE(end_date)
),


cte_appmetrics as(
    select distinct
    day,
    os_type,
    visitor_id, 
    app_page_type, 
    SPLIT(product_list_split, ';')[safe_offset(1)] as product_id
    from cte_appmetrics_raw, UNNEST(product_list_split) product_list_split
),


pdpvisits as ( 
    select day, os_type, product_id, count(visitor_id) as visits
    from ( 
    select distinct day, os_type, visitor_id, product_id from cte_appmetrics 
    where app_page_type = 'prodView' or app_page_type='pdp_visible_components'
    ) 
    group by 1,2,3
), 


impressions as(
  select 
  day, 
  os_type,
  page_id as product_id,
  count(distinct concat(visit_high, visit_low)) as impression
  FROM `falabella-search-prod.recommendation.facl_widget_impression` 
  WHERE source='App'
  AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
  group by 1,2,3
),

click_raw as(
    select distinct
    day,
    os_type,
    product_id,
    concat(visit_high, visit_low) as visitor_id
    FROM `falabella-search-prod.recommendation.facl_widget_click`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

click as(
    select 
    day, 
    os_type,
    product_id,
    count(visitor_id) as clicks
    FROM click_raw
    group by 1,2,3
),


cartadd as(
  select t1.day, t1.os_type, t1.product_id, count(t1.visitor_id) as cartadds
  FROM (
    select distinct
    day,
    os_type,
    product_id,
    concat(visit_high, visit_low) as visitor_id
    FROM `falabella-search-prod.recommendation.facl_widget_cartadd`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  group by 1,2,3
),


revenue_order as (
    SELECT  
    day,
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    purchase_id,
    product_id,
    sum(quantity) quantity,
    sum(safe_multiply(price, quantity)) as revenue
    FROM `falabella-search-prod.recommendation.facl_omniture_orders` 
    WHERE day BETWEEN DATE(start_date) and DATE(end_date)
    group by 1,2,3,4
),


orders_ as(
  select t1.day, t1.os_type, t1.product_id, count(distinct purchase_id) as orders, sum(revenue) as revenue, sum(quantity) quantity
  FROM (
    select distinct
    day,
    os_type,
    product_id,
    concat(visit_high, visit_low) as visitor_id
    FROM `falabella-search-prod.recommendation.facl_widget_orders`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  inner join revenue_order t3
  on (t1.day=t3.day and t1.visitor_id=t3.visitor_id and t1.product_id=t3.product_id)
  group by 1,2,3
),


cat_mapping as (
    select t1.product_id, t2.category 
    from (
        select distinct product_id from pdpvisits
    )t1
    inner join (
        select distinct 
        product_id, 
        split(split(PRODUCT_L0_CATEGORY_PATHS, '||')[safe_offset(1)], ',')[safe_offset(0)] as category 
        from `tc-sc-bi-bigdata-dtl-fcom-prd.cl_entities.facl_prod_feed`
        WHERE DATE(_PARTITIONTIME) = DATE(end_date)
    ) t2
    on (t1.product_id=t2.product_id)
)


select
t1.day,
t1.os_type,
category,
t1.product_id,
visits,
impression,
clicks,
cartadds,
orders,
revenue, 
quantity
from pdpvisits t1
left join impressions t2 on (t1.day = t2.day and t1.os_type = t2.os_type and t1.product_id = t2.product_id)
left join click t3 on (t1.day = t3.day and t1.os_type = t3.os_type and t1.product_id = t3.product_id)
left join cartadd t4 on (t1.day = t4.day and t1.os_type = t4.os_type and t1.product_id = t4.product_id)
left join orders_ t5 on (t1.day = t5.day and t1.os_type = t5.os_type and t1.product_id = t5.product_id)
left join cat_mapping t6 on (t1.product_id = t6.product_id)
where category is not null and visits is not null 
 




