DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 
DECLARE widget_ids STRING DEFAULT '(más_opciones_similares|varias_personas_después_miran|varias personas después miran|más opciones similares)';


-- insert into `core-search-dev.recommendation_dashboard.page_view`
create table if not exists `core-search-dev.recommendation_dashboard.page_view` PARTITION BY day as
with cte_appvisits as ( 
    select distinct 
    concat(post_visid_high, post_visid_low) as visitor_id, 
    case when LOWER(user_agent) like '%iphone%' then 'iOS' else 'Android' end as os_type,
    day,
    app_page_type, 
    coalesce(purchaseid, post_purchaseid) as purchase_id,
    product_list 
    from `falabella-search-prod.relevancy_matrices.facl_raw_omniture` 
    where ((LOWER(user_agent) like '%iphone%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.2.16') OR 
    (LOWER(user_agent) like '%android%' AND split(coalesce(mobileappid,post_mobileappid),' ')[safe_offset(1)]>'2.3.0'))
    and day between DATE(start_date) AND DATE(end_date)
),


pdpvisits as( 
    select day, os_type, count(distinct visitor_id) as visits
    from ( 
        select distinct os_type, day, visitor_id from cte_appvisits 
        where app_page_type = 'prodView' or app_page_type = 'pdp_visible_components'
    ) 
    group by 1, 2
), 

 
impressions as(
  select 
  day, 
  os_type,
  count(distinct concat(visit_high, visit_low)) as impression
  FROM `falabella-search-prod.recommendation.facl_widget_impression` 
  WHERE source='App'
  AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
  group by 1,2
),

click_raw as(
    select distinct
    day,
    os_type,
    concat(visit_high, visit_low) as visitor_id,
    product_id
    FROM `falabella-search-prod.recommendation.facl_widget_click`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

click as(
    select 
    day, 
    os_type,
    count(distinct visitor_id) as clicks
    FROM click_raw
    group by 1,2
),


cartadd as(
  select t1.day, t1.os_type, count(distinct t1.visitor_id) as cartadds
  FROM (
    select distinct
    day,
    os_type,
    concat(visit_high, visit_low) as visitor_id,
    product_id,
    FROM `falabella-search-prod.recommendation.facl_widget_cartadd`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  group by 1,2
),


revenue_order as (
    SELECT  
    day,
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    purchase_id,
    product_id,
    sum(quantity) quantity,
    sum(safe_multiply(price, quantity)) as revenue
    FROM `falabella-search-prod.recommendation.facl_omniture_orders` 
    WHERE day BETWEEN DATE(start_date) and DATE(end_date)
    group by 1,2,3,4
),


orders_ as(
  select t1.day, t1.os_type, count(distinct purchase_id) as orders, sum(revenue) as revenue, sum(quantity) total_quantity_bought
  FROM (
    select distinct
    day,
    os_type,
    concat(visit_high, visit_low) as visitor_id,
    product_id
    FROM `falabella-search-prod.recommendation.facl_widget_orders`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  inner join revenue_order t3
  on (t1.day=t3.day and t1.visitor_id=t3.visitor_id and t1.product_id=t3.product_id)
  group by 1,2
)


select 
pdpvisits.day,
pdpvisits.os_type,
visits,
impression,
clicks,
cartadds,
orders,
revenue,
total_quantity_bought
from pdpvisits
left join impressions on (pdpvisits.day=impressions.day and pdpvisits.os_type=impressions.os_type)
left join click on (pdpvisits.day=click.day and pdpvisits.os_type=click.os_type)
left join cartadd on (pdpvisits.day=cartadd.day and pdpvisits.os_type=cartadd.os_type)
left join orders_ on (pdpvisits.day=orders_.day and pdpvisits.os_type=orders_.os_type)
 