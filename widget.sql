DECLARE start_date STRING DEFAULT '2022-06-01';
DECLARE end_date STRING DEFAULT '2023-02-11'; 
DECLARE widget_ids STRING DEFAULT '(más_opciones_similares|varias_personas_después_miran|varias personas después miran|más opciones similares)';


-- insert into `core-search-dev.recommendation_dashboard.widget_view`
create table if not exists `core-search-dev.recommendation_dashboard.widget_view` PARTITION BY day as
with impressions as(
  select 
  day, 
  os_type,
  widget_id,
  count(distinct concat(visit_high, visit_low)) as impression
  FROM `falabella-search-prod.recommendation.facl_widget_impression` 
  WHERE source='App'
  AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
  AND REGEXP_CONTAINS(widget_id, widget_ids)
  AND day BETWEEN DATE(start_date) AND DATE(end_date)
  group by 1,2,3
),

click_raw as(
    select distinct
    day,
    os_type,
    widget_id,
    concat(visit_high, visit_low) as visitor_id,
    product_id
    FROM `falabella-search-prod.recommendation.facl_widget_click`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
),

click as(
    select 
    day, 
    os_type,
    widget_id,
    count(distinct visitor_id) as clicks
    FROM click_raw
    group by 1,2,3
),


cartadd as(
  select t1.day, t1.os_type, t1.widget_id, count(distinct t1.visitor_id) as cartadds
  FROM (
    select distinct
    day,
    os_type,
    widget_id,
    concat(visit_high, visit_low) as visitor_id,
    product_id,
    FROM `falabella-search-prod.recommendation.facl_widget_cartadd`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  group by 1,2,3
),


revenue_order as (
    SELECT  
    day,
    CONCAT(post_visid_high, post_visid_low) as visitor_id,
    purchase_id,
    product_id,
    sum(quantity) quantity,
    sum(safe_multiply(price, quantity)) as revenue
    FROM `falabella-search-prod.recommendation.facl_omniture_orders` 
    WHERE day BETWEEN DATE(start_date) and DATE(end_date)
    group by 1,2,3,4
),


orders_ as(
  select t1.day, t1.os_type, t1.widget_id, count(distinct purchase_id) as orders, sum(revenue) as revenue, sum(quantity) total_quantity_bought
  FROM (
    select distinct
    day,
    os_type,
    widget_id,
    concat(visit_high, visit_low) as visitor_id,
    product_id
    FROM `falabella-search-prod.recommendation.facl_widget_orders`
    WHERE source='App'
    AND ((os_type = 'Android' and  app_version >= '2.3.0') or (os_type = 'iOS' and  app_version >= '2.2.16'))
    AND REGEXP_CONTAINS(widget_id, widget_ids)
    AND day BETWEEN DATE(start_date) AND DATE(end_date)
  )t1
  inner join click_raw t2
  on (t1.day=t2.day and t1.os_type=t2.os_type and t1.visitor_id=t2.visitor_id and t1.product_id=t2.product_id)
  inner join revenue_order t3
  on (t1.day=t3.day and t1.visitor_id=t3.visitor_id and t1.product_id=t3.product_id)
  group by 1,2,3
)


select 
impressions.day,
impressions.os_type,
impressions.widget_id,
impression,
clicks,
cartadds,
orders,
revenue,
total_quantity_bought
from impressions
left join click on (impressions.day=click.day and impressions.os_type=click.os_type and impressions.widget_id=click.widget_id)
left join cartadd on (impressions.day=cartadd.day and impressions.os_type=cartadd.os_type and impressions.widget_id=cartadd.widget_id)
left join orders_ on (impressions.day=orders_.day and impressions.os_type=orders_.os_type and impressions.widget_id=orders_.widget_id)
 